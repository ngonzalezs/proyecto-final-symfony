<?php

namespace App\Entity;

use App\Repository\ImagenesRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ImagenesRepository::class)
 */
class Imagenes
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $descripcion;

    /**
     * @ORM\Column(type="integer")
     */
    private $num_visualizaciones;

    /**
     * @ORM\Column(type="integer")
     */
    private $num_likes;

    /**
     * @ORM\Column(type="integer")
     */
    private $num_descargas;

    /**
     * @ORM\ManyToOne(targetEntity=Categorias::class, inversedBy="imagenes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $categoria;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->descripcion;
    }

    public function setDescripcion(string $descripcion): self
    {
        $this->descripcion = $descripcion;

        return $this;
    }

    public function getNumVisualizaciones(): ?int
    {
        return $this->num_visualizaciones;
    }

    public function setNumVisualizaciones(int $num_visualizaciones): self
    {
        $this->num_visualizaciones = $num_visualizaciones;

        return $this;
    }

    public function getNumLikes(): ?int
    {
        return $this->num_likes;
    }

    public function setNumLikes(int $num_likes): self
    {
        $this->num_likes = $num_likes;

        return $this;
    }

    public function getNumDescargas(): ?int
    {
        return $this->num_descargas;
    }

    public function setNumDescargas(int $num_descargas): self
    {
        $this->num_descargas = $num_descargas;

        return $this;
    }

    public function getCategoria(): ?Categorias
    {
        return $this->categoria;
    }

    public function setCategoria(?Categorias $categoria): self
    {
        $this->categoria = $categoria;

        return $this;
    }
}
