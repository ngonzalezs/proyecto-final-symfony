<?php

namespace App\Repository;

use App\Entity\Asociados;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Asociados|null find($id, $lockMode = null, $lockVersion = null)
 * @method Asociados|null findOneBy(array $criteria, array $orderBy = null)
 * @method Asociados[]    findAll()
 * @method Asociados[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AsociadosRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Asociados::class);
    }


    public function mostrarAsociados(){
        $consulta = $this->getEntityManager()->createQuery(
            'SELECT a.nombre, a.logo  FROM App:Asociados a'
        );
        //$resultadoConsulta = $consulta->getResult();
        return $consulta->getResult();
    }

    // /**
    //  * @return Asociados[] Returns an array of Asociados objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Asociados
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
