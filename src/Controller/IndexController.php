<?php

namespace App\Controller;

use App\Entity\Asociados;
use App\Entity\Imagenes;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    #[Route('/', name: 'index')]
    public function index(): Response
    {
        //Obtenemos el array de los asociados para mostrarlo en el index.
        $entityManager = $this->getDoctrine()->getManager();
        $arrayAsociados = $entityManager->getRepository(Asociados::class)->mostrarAsociados();
        shuffle($arrayAsociados);

        //Mostramos las imagenes en su respectiva categoria

        $arrayImagenesCategoria1 = $entityManager->getRepository(Imagenes::class)->mostrarImagenesPorCategoria(1);
        $arrayImagenesCategoria2 = $entityManager->getRepository(Imagenes::class)->mostrarImagenesPorCategoria(2);
        $arrayImagenesCategoria3 = $entityManager->getRepository(Imagenes::class)->mostrarImagenesPorCategoria(3);
        
        $entityManager->flush();
        return $this->render('index/index.html.twig', [
            'controller_name' => 'IndexController',
            'asociados' => array_splice($arrayAsociados,0,3),
            'imagenesC1' => $arrayImagenesCategoria1,
            'imagenesC2' => $arrayImagenesCategoria2,
            'imagenesC3' => $arrayImagenesCategoria3
        ]);
    }
}
