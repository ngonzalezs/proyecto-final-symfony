<?php

namespace App\Controller;

use App\Entity\Asociados;
use App\Form\AsociadosType;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

class AsociadosController extends AbstractController
{
    #[Route('/asociados', name: 'asociados')]
    public function index(Request $request,SluggerInterface $slugger,LoggerInterface $log): Response
    {
        $asociados = new Asociados();
        $form = $this->createForm(AsociadosType::class,$asociados);
        $form->handleRequest($request);

        $entityManager = $this->getDoctrine()->getManager();
        $arrayAsociados = $entityManager->getRepository(Asociados::class)->findAll();

        if(($form->isSubmitted()) && ($form->isValid())) {
            $imageFile = $form->get('logo')->getData();
            if($imageFile){
                $originalFilename = pathinfo($imageFile->getClientOriginalName(),PATHINFO_FILENAME);
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$imageFile->guessExtension();

                // Move the file to the directory where photos are stored
                try {
                    $imageFile->move(
                        $this->getParameter('images_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    throw new \Exception('Error al subir la imagen');
                }
                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $asociados->setLogo($newFilename);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($asociados);

            $log->info("Se ha subido un logo con el nombre".$asociados->getNombre());

            $entityManager->flush();
            $this->addFlash("exito", "Imagen subida con exito");
            return $this->redirectToRoute('asociados');
        }



        return $this->render('asociados/index.html.twig', [
            'controller_name' => 'AsociadosController',
            'formulario'=> $form->createView(),
            'asociados'=>$arrayAsociados
        ]);
    }
}
