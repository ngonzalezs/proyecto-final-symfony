<?php

namespace App\Controller;

use App\Entity\Imagenes;
use App\Form\ImagenesType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\String\Slugger\SluggerInterface;

class GaleriaController extends AbstractController
{
    #[Route('/galeria', name: 'galeria')]
    public function index(Request $request,SluggerInterface $slugger): Response
    {
        $imagenes = new Imagenes();
        $form = $this->createForm(ImagenesType::class,$imagenes);
        $form->handleRequest($request);

        $entityManager = $this->getDoctrine()->getManager();

        $arrayImagenes = $entityManager->getRepository(Imagenes::class)->findAll();


        if(($form->isSubmitted()) && ($form->isValid())) {

            //Ponemos por defecto los valores de los likes,descargas y visualizaciones
            $imagenes->setNumDescargas(0);
            $imagenes->setNumLikes(0);
            $imagenes->setNumVisualizaciones(0);

            $imageFile = $form->get('nombre')->getData();
            if($imageFile){
                $originalFilename = pathinfo($imageFile->getClientOriginalName(),PATHINFO_FILENAME);
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename.'-'.uniqid().'.'.$imageFile->guessExtension();

                // Move the file to the directory where photos are stored
                try {
                    $imageFile->move(
                        $this->getParameter('images_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {
                    throw new \Exception('Error al subir la imagen');
                }
                // updates the 'brochureFilename' property to store the PDF file name
                // instead of its contents
                $imagenes->setNombre($newFilename);
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($imagenes);

            $entityManager->flush();
            $this->addFlash("exito", "Imagen subida con exito");
            return $this->redirectToRoute('galeria');
        }
        return $this->render('galeria/index.html.twig', [
            'controller_name' => 'GaleriaController',
            'formulario' => $form->createView(),
            'imagenes' => $arrayImagenes
        ]);
    }
}
