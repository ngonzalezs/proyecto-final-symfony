<?php

namespace App\Controller;

use App\Entity\Mensajes;
use App\Entity\User;
use App\Form\MensajesType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ContactController extends AbstractController
{
    #[Route('/contact', name: 'contact')]
    public function index(Request $request,\Swift_Mailer $mailer): Response
    {
        $mensajes = new Mensajes();
        $form = $this->createForm(MensajesType::class,$mensajes);
        $form->handleRequest($request);

        $entityManager = $this->getDoctrine()->getManager();
        if(($form->isSubmitted()) && ($form->isValid())) {

            //Ponemos la fecha por defecto al dia de hoy.
            $mensajes->setFecha(new \DateTime("now"));
            $mensajes->setEmail($this->getUser()->getUsername());

            //Enviamos el mensaje al correo indicado.
            $message = (new \Swift_Message("Datos del Formulario"))
                ->setFrom($this->getUser()->getUsername())
                ->setTo('noahpluto2@gmail.com')
                ->setBody(
                    $mensajes->getNombre().'  '.$mensajes->getApellidos().'  '.$mensajes->getEmail().'  '.$mensajes->getAsunto().'  '.$mensajes->getTexto()
                    ,
                    'text/plain'
                );
            $mailer->send($message);

            $entityManager->persist($mensajes);
            $entityManager->flush();
            return $this->redirectToRoute('contact');
        }


        return $this->render('contact/index.html.twig', [
            'controller_name' => 'ContactController',
            'formulario'=> $form->createView(),
        ]);
    }
}
